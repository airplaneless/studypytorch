import torch
import torchvision
import torchvision.transforms as transforms

import numpy as np

import torch.utils.data
import torch.nn as nn
import torch.nn.functional as F


class SegNet(nn.Module):

    def __init__(self, nfilters, in_channels, ker_size):
        super(SegNet, self).__init__()
        # 3x256x256 -> Nx128x128
        self.pad1_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv1_1 = nn.Conv2d(in_channels, nfilters, ker_size)
        self.pad1_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv1_2 = nn.Conv2d(nfilters, nfilters, ker_size)
        self.pool1 = nn.MaxPool2d(2, 2)
        # Nx128x128 -> 2Nx64x64
        self.pad2_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv2_1 = nn.Conv2d(nfilters, nfilters*2, ker_size)
        self.pad2_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv2_2 = nn.Conv2d(nfilters*2, nfilters*2, ker_size)
        self.pool2 = nn.MaxPool2d(2, 2)
        # 2Nx64x64 -> 4Nx32x32
        self.pad3_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv3_1 = nn.Conv2d(nfilters*2, nfilters*4, ker_size)
        self.pad3_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv3_2 = nn.Conv2d(nfilters*4, nfilters*4, ker_size)
        self.pool3 = nn.MaxPool2d(2, 2)
        # 4Nx32x32 -> 8Nx16x16
        self.pad4_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv4_1 = nn.Conv2d(nfilters*4, nfilters*8, ker_size)
        self.pad4_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv4_2 = nn.Conv2d(nfilters*8, nfilters*8, ker_size)
        self.pool4 = nn.MaxPool2d(2, 2)
        # 8Nx16x16 -> 4Nx32x32
        self.pad_up1_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up1_1 = nn.Conv2d(nfilters*8, nfilters*8, ker_size)
        self.pad_up1_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up1_2 = nn.Conv2d(nfilters*8, nfilters*4, ker_size)
        # 4Nx32x32 -> 2Nx64x64
        self.pad_up2_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up2_1 = nn.Conv2d(nfilters*4, nfilters*4, ker_size)
        self.pad_up2_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up2_2 = nn.Conv2d(nfilters*4, nfilters*2, ker_size)
        # 2Nx64x64 -> Nx128x128
        self.pad_up3_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up3_1 = nn.Conv2d(nfilters*2, nfilters*2, ker_size)
        self.pad_up3_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up3_2 = nn.Conv2d(nfilters*2, nfilters, ker_size)
        # Nx128x128 -> 1x256x256
        self.pad_up4_1 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up4_1 = nn.Conv2d(nfilters, nfilters, ker_size)
        self.pad_up4_2 = nn.ReflectionPad2d(int(ker_size/2))
        self.conv_up4_2 = nn.Conv2d(nfilters, 1, ker_size)

    def forward(self, x):
        x = self.conv1_1(self.pad1_1(x))
        x = F.relu(x)
        x = self.conv1_2(self.pad1_2(x))
        x = F.relu(x)
        x = self.pool1(x)

        x = self.conv2_1(self.pad2_1(x))
        x = F.relu(x)
        x = self.conv2_2(self.pad2_2(x))
        x = F.relu(x)
        x = self.pool2(x)

        x = self.conv3_1(self.pad3_1(x))
        x = F.relu(x)
        x = self.conv3_2(self.pad3_2(x))
        x = F.relu(x)
        x = self.pool3(x)

        x = self.conv4_1(self.pad4_1(x))
        x = F.relu(x)
        x = self.conv4_2(self.pad4_2(x))
        x = F.relu(x)
        x = self.pool4(x)

        x = F.interpolate(x, scale_factor=2, mode='bilinear', align_corners=True)
        x = self.conv_up1_1(self.pad_up1_1(x))
        x = F.relu(x)
        x = self.conv_up1_2(self.pad_up1_2(x))
        x = F.relu(x)

        x = F.interpolate(x, scale_factor=2, mode='bilinear', align_corners=True)
        x = self.conv_up2_1(self.pad_up2_1(x))
        x = F.relu(x)
        x = self.conv_up2_2(self.pad_up2_2(x))
        x = F.relu(x)

        x = F.interpolate(x, scale_factor=2, mode='bilinear', align_corners=True)
        x = self.conv_up3_1(self.pad_up3_1(x))
        x = F.relu(x)
        x = self.conv_up3_2(self.pad_up3_2(x))
        x = F.relu(x)

        x = F.interpolate(x, scale_factor=2, mode='bilinear', align_corners=True)
        x = self.conv_up4_1(self.pad_up4_1(x))
        x = F.relu(x)
        x = self.conv_up4_2(self.pad_up4_2(x))
        x = torch.sigmoid(x)
        return x

if __name__ == '__main__':
    net = SegNet(32, 3, 3)
    img = torch.rand(1, 3, 256, 256)
    output = net(img)
    print(output.shape)