# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = "0"

import torch
import torchvision
import torchvision.transforms as transforms

import numpy as np
import pickle

# torch.cuda.get_device_name(0)

import torch.optim as optim
import torch.utils.data
import torch.nn as nn
import torch.nn.functional as F

import hyperopt
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials

from tqdm import tqdm

# DATA_PATH = "/home/u0052/heartmri/datasets/LV10slices.npy"
DATA_PATH = "/home/airplaneless/source/mri/datasets/LV10slices.npy"


class ConvBlock(nn.Module):

    def __init__(self, in_channels, out_channels, ker_size, pd, blockNum):
        super(ConvBlock, self).__init__()
        self.blockNum = blockNum

        self.blockListPad = []
        self.blockListConv = []
        self.blockListBNorm = []

        self.blockListPad.append(nn.ReflectionPad2d(int(ker_size / 2)))
        self.blockListConv.append(nn.Conv2d(in_channels, out_channels, ker_size))
        self.blockListBNorm.append(nn.BatchNorm2d(out_channels))

        for block in range(blockNum-1):
            self.blockListPad.append(nn.ReflectionPad2d(int(ker_size / 2)))
            self.blockListConv.append(nn.Conv2d(out_channels, out_channels, ker_size))
            self.blockListBNorm.append(nn.BatchNorm2d(out_channels))

        self.pool = nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout2d(p=pd)

    def forward(self, x):
        for i in range(self.blockNum):
            x = self.blockListPad[i](x)
            x = self.blockListConv[i](x)
            x = self.blockListBNorm[i](x)
            x = F.relu(x)
        up = F.relu(x)
        down = self.pool(up)
        down = self.dropout(down)

        return up, down


class UpConvBlock(nn.Module):

    def __init__(self, in_channels, out_channels, ker_size, pd, blockNum):
        super(UpConvBlock, self).__init__()
        self.blockNum = blockNum

        self.blockListPad = []
        self.blockListConv = []

        self.blockListPad.append(nn.ReflectionPad2d(int(ker_size / 2)))
        self.blockListConv.append(nn.Conv2d(in_channels, out_channels, ker_size))

        for block in range(blockNum - 1):
            self.blockListPad.append(nn.ReflectionPad2d(int(ker_size / 2)))
            self.blockListConv.append(nn.Conv2d(out_channels, out_channels, ker_size))

        self.dropout = nn.Dropout2d(p=pd)

    def forward(self, x):
        x = self.dropout(x)
        for i in range(self.blockNum):
            x = self.blockListPad[i](x)
            x = self.blockListConv[i](x)
            x = F.relu(x)

        return x


class Unet(nn.Module):

    # 256x256

    def __init__(self, nfilters, kersize, blockNum, convNum, drpt):
        super(Unet, self).__init__()
        self.nf = nfilters
        self.ks = kersize
        self.blockNum = blockNum

        self.cblockList = []

        self.cblockList.append(ConvBlock(1, self.nf, self.ks, 0.2, convNum))
        for i in range(blockNum):
            self.cblockList.append(ConvBlock(self.nf * (2 ** i), self.nf * (2 ** (i+1)), self.ks, drpt, convNum))

        # middle
        self.pad1 = nn.ReflectionPad2d(int(self.ks / 2))
        self.conv1 = nn.Conv2d(self.nf * (2 ** (i+1)), self.nf * (2 ** (i+1)), self.ks)
        self.pad2 = nn.ReflectionPad2d(int(self.ks / 2))
        self.conv2 = nn.Conv2d(self.nf * (2 ** (i+1)), self.nf * (2 ** (i+1)), self.ks)

        self.upConvList = []
        self.upblockList = []

        for i in range(blockNum, 0, -1):
            self.upConvList.append(nn.ConvTranspose2d(self.nf * (2 ** i), self.nf * (2 ** i), 2, 2).cpu())
            self.upblockList.append(UpConvBlock(self.nf * (2 ** i) * 2, self.nf * (2 ** (i-1)), self.ks, drpt, convNum))
        self.upConvList.append(nn.ConvTranspose2d(self.nf, self.nf, 2, 2).cpu())
        self.upblockList.append(UpConvBlock(self.nf * 2, self.nf, self.ks, drpt, convNum))

        self.pad3 = nn.ReflectionPad2d(int(self.ks / 2))
        self.conv1x1 = nn.Conv2d(self.nf, 1, self.ks)

    def forward(self, x):
        
        xl = list()
        down = x
        for block in self.cblockList:
            x, down = block(down)
            xl.append(x)

        xm = self.conv1(self.pad1(down))
        xm = F.relu(xm)
        xm = self.conv2(self.pad2(xm))
        ux = F.relu(xm)

        for i in range(len(self.upConvList)):
            ux = self.upConvList[i](ux)
            ux = torch.cat([ux, xl[-1-i]], 1)
            ux = self.upblockList[i](ux)

        ux = self.conv1x1(self.pad3(ux))

        return torch.sigmoid(ux)


class ACDCdataset(torch.utils.data.Dataset):

    def __init__(self, transform=None):
        x, y = np.load(DATA_PATH)
        nsamples = x.shape[0]
        img_size = x.shape[1]
        x = x[:, :, :, :].swapaxes(3, 1).reshape((nsamples * 10, img_size, img_size))
        y = y[:, :, :, :].swapaxes(3, 1).reshape((nsamples * 10, img_size, img_size))
        x = self.getReshapedImages(x)
        y = self.getReshapedImages(y)
        self.images = x
        self.masks = y
        self.transform = transform

    @staticmethod
    def getReshapedImages(arr):
        dim = arr.shape
        return arr.reshape((dim[0], dim[1], dim[2], 1))

    def __len__(self):
        return len(self.masks)

    def __getitem__(self, i):
        img = self.images[i]
        mask = self.masks[i]
        if self.transform:
            img = self.transform(img)
            mask = self.transform(mask)
        return (img, mask)


def dice_loss(inputs, target):

    iflat = inputs.view(-1)
    tflat = target.view(-1)
    intersection = (iflat * tflat).sum()

    return 1 - ((2. * intersection) / (iflat.sum() + tflat.sum()))


def trainAndGetLoss(params):
    print(params)
    transform = transforms.Compose([
        transforms.ToTensor()
    ])

    dataset = ACDCdataset(transform=transform)

    datagenerator = torch.utils.data.DataLoader(dataset=dataset, batch_size=8, shuffle=True)

    net = Unet(params[0], params[1], params[2], params[3], params[4]).cpu()
    optimizer = optim.Adam(net.parameters())
    criterion = dice_loss

    EPOCHS=60
    
    epochs = range(EPOCHS)
    losses = np.zeros(EPOCHS)

    net.train()

    for epoch in epochs:
        batches = tqdm(datagenerator)
        for batch in batches:
            imgs, masks = batch

            optimizer.zero_grad()
            outputs = net(imgs.cpu().float())
            loss = criterion(outputs.float(), masks.cpu().float())
            loss.backward()
            optimizer.step()

            losses[epoch] += loss.item()

            batches.set_description("loss: {}".format(round(loss.item(), 4)))

        print("\nloss: {}\n".format(round(losses[epoch] / len(batches), 3)))

    return losses[epoch]


def f(x):
    res = trainAndGetLoss([x['fn'], x['ks'], x['blockNum'], x['convNum'], x['drpt']])
    return res


if __name__ == '__main__':

    for param in params:
        net = YourNet(params)
        # Любой тензор размером с картинку
        tensor = torch.randn((1,3,32,32))
        output = net(tensor)
        if tensor.shape == output.shape:
            return 'Ok!'
        else:
            return 'wrong'


    # space = {
    #     'fn': hp.choice('fn', np.arange(3, 64)),
    #     'ks': hp.choice('ks', [3, 5, 7]),
    #     'blockNum': hp.choice('blockNum', [1, 2, 3, 4, 5, 6, 7]),
    #     'convNum': hp.choice('convNum', [1, 2, 3, 4]),
    #     'drpt': hp.uniform('drpt', 0.2, 0.8)
    # }

    # trials = Trials()
    # best = fmin(f, space, algo=tpe.suggest, max_evals=90, trials=trials, verbose=1)

    # print("best: {}".format(best))

    # with open('trials.pkl', 'wb') as f:
    #     pickle.dump(trials, f)

